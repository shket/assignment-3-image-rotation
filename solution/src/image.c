#include "image.h"
#include <stdlib.h>

struct image image_create(size_t width, size_t height) {
    struct image image = {.width = width, .height = height};
    image.data = malloc(sizeof(struct pixel) * width * height);
    return image;
}

void image_destroy(const struct image* image) {
    free(image->data);
}
