#include "bmp.h"
#include <stdlib.h>

const uint8_t PADDING_BYTES[4] = {0};

size_t get_padding(size_t width) {
    return 4 - width * 3 % 4;
}

size_t get_size(size_t width, size_t height) {
    size_t padding = get_padding(width);
    return (width * sizeof(struct pixel) + padding) * height;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, in))
        return READ_INVALID_HEADER;

    size_t padding = get_padding(header.biWidth);
    *img = image_create(header.biWidth, header.biHeight);

    for (size_t i = 0; i < img->height; i++) {
        if (!fread(img->data + i * img->width, sizeof(struct pixel), img->width,
                   in))
            return READ_INVALID_BITS;
        if (fseek(in, (long)padding, SEEK_CUR))
            return READ_INVALID_BITS;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    size_t padding = get_padding(img->width);
    size_t size = get_size(img->width, img->height);

    struct bmp_header header = {
        .bfType = BF_TYPE,
        .bfileSize = size + B_OFF_BITS,
        .bfReserved = BF_RESERVED,
        .bOffBits = B_OFF_BITS,
        .biSize = BI_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = BI_PLANES,
        .biBitCount = BI_BIT_COUNT,
        .biCompression = BI_COMPRESSION,
        .biSizeImage = size,
        .biXPelsPerMeter = BI_X_PELS_PER_METER,
        .biYPelsPerMeter = BI_Y_PELS_PER_METER,
        .biClrUsed = BI_CLR_USED,
        .biClrImportant = BI_CLR_IMPORTANT,
    };
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof(struct pixel),
                    img->width, out))
            return WRITE_ERROR;
        if (!fwrite(&PADDING_BYTES, 1, padding, out))
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
