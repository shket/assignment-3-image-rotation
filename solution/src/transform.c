#include "transform.h"

struct image rotate(struct image const source) {
    struct image transformed = image_create(source.height, source.width);
    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            transformed.data[(j + 1) * source.height - i - 1] =
                source.data[i * source.width + j];
        }
    }
    return transformed;
}
