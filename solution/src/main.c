#include <stdio.h>

#include "bmp.h"
#include "image.h"
#include "transform.h"
#include "util.h"

int main(int argc, char** argv) {
    if (argc != 3)
        error("Usage: ./" EXECUTABLE_NAME " SOURCE_IMAGE TRANSFORMED_IMAGE");

    FILE* input_file = fopen(argv[1], "rb");
    if (!input_file)
        error("Bad input file");
    FILE* output_file = fopen(argv[2], "wb");
    if (!output_file) {
        fclose(input_file);
        error("Bad output file");
    }

    struct image source_image = {0};
    if (from_bmp(input_file, &source_image))
        error("Bmp reading error");

    struct image transformed_image = rotate(source_image);
    if (to_bmp(output_file, &transformed_image))
        error("Bmp writting error");

    fclose(input_file);
    fclose(output_file);
    image_destroy(&source_image);
    image_destroy(&transformed_image);

    return 0;
}
