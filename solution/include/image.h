#ifndef IMAGE_H
#define IMAGE_H

#include <stddef.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(size_t width, size_t height);

void image_destroy(const struct image* image);

#endif
